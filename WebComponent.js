class MiBoton extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click',function(e){
            alert('hola');
        })
    }
}

customElements.define('mi-boton',MiBoton);

customElements.define('mi-boton-2',class extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click',function(e){
            alert('Hola, soy boton 2')
        })
    }
})

class MiBotonExtendido extends HTMLButtonElement{
    constructor() {
        super();
        this.addEventListener('click',(e)=>{
            console.log("evento click: " + this.innerHTML);
            alert('MiBotonExtendido');
        });
    }
    
    static get ce_Name(){
        return 'mi-boton-extendido';
    }

    get is (){
        return this.getAttribute('is');
    }

    set is (value){
        this.setAttribute('is', value|| this.ce_Name);
    }

    
}

customElements.define('mi-boton-extendido',MiBotonExtendido,{extends: 'button'});






const miBotonExtendido2 = document.createElement('button',{is: MiBotonExtendido.ce_Name});

miBotonExtendido2.textContent = "Soy un mi-boton-extendido";
document.body.appendChild(miBotonExtendido2);

const miBotonExtendido3 = document.createElement('button',{is:MiBotonExtendido.ce_Name});
miBotonExtendido3.textContent="hola-btn3";
document.querySelector('#container').appendChild(miBotonExtendido3);



class miMensaje extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click',function(e){
            alert('Click en mensaje');
        });
        console.log('constructor: cuando el elemento es creado');
    }

    static get observedAttributes(){
        return['msj','casi-visible'];
    }

    get msj(){
        return this.getAttribute('msj')
    }

    set msj(val){
        this.setAttribute('msj',val)
    }

    get casiVisible(){
        return this.hasAttribute('casi-visible');
    }

    set casiVisible(value){
        if(value){
            this.setAttribute('casi-visible',' ')
        }else{
            this.removeAttribute('casi-visible')
        }
    }

    setCasiVisible(){
        if(this.casiVisible){
            this.style.opacity=0.1;
        }else{
            this.style.opacity = 1
        }
    }

    connectedCallback(){
        console.log('connectecCallBack: Cuando el elemento es insertado en el documento');
    }

    disconnectedCallback(){
        alert('disconnectedCallBack: Cando el elemento es eliminado del documento');
    }

    adoptedCallBack(){
        alert('adoptedCallback: Cuando el elemento es adoptado por otro documento');
    }

    attributeChangedCallback(attrName,oldVal,newVal){
        console.log('attributeChangedCallBack: Cuando cambia un atributo');
        if(attrName === 'msj'){
            this.pintarMensaje(newVal);
        }
        if(attrName === 'casi-visible'){
            this.setCasiVisible()
        }
    }

    pintarMensaje (msj){
        this.innerHTML = msj;
    }


}

customElements.define('mi-mensaje',miMensaje);


let myMensaje = document.createElement('mi-mensaje');
myMensaje.msj ='Otro mensaje';
document.body.appendChild(myMensaje);

let tercerMensaje = new miMensaje();
tercerMensaje.msj ='Tercer mensaje';
document.body.appendChild(tercerMensaje);

console.log(typeof(null))